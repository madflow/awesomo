package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/madflow/awesomo/config"
	"github.com/madflow/awesomo/model"
)

func GitlabRepo(repoArg string, cfg config.Config) model.AwesomeRepo {

	repoUrl, err := url.Parse(repoArg)
	if err != nil {
		panic(err)
	}

	var gitlabApiBaseUrl string

	if cfg.Gitlab.ApiUrl != "" {
		gitlabApiBaseUrl = cfg.Gitlab.ApiUrl
	} else {
		gitlabApiBaseUrl = "https://gitlab.com/api/v4"
	}

	var gitlabApiRepos string = fmt.Sprintf("%s/projects", gitlabApiBaseUrl)

	gitlabApiUrl, err := url.Parse(gitlabApiRepos)

	if err != nil {
		panic(err)
	}

  projectSlug := url.PathEscape(strings.Trim(repoUrl.Path, "/"))
	repoApiUrl := url.URL{Scheme: gitlabApiUrl.Scheme, Host: gitlabApiUrl.Host, Path: gitlabApiUrl.Path}

	client := &http.Client{}
	req, _ := http.NewRequest("GET", repoApiUrl.String() + "/" + projectSlug, http.NoBody)
	if cfg.Gitlab.ApiKey != "" {
		req.Header.Set("Authorization", fmt.Sprintf("token %s", cfg.Gitlab.ApiKey))
	}
	repoResp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer repoResp.Body.Close()
	body, err := io.ReadAll(repoResp.Body)


	if err != nil {
		panic(err)
	}

	var repoModel model.GitlabProject

	if err := json.Unmarshal(body, &repoModel); err != nil {
		panic(err)
	}

	awesomeRepo := model.AwesomeRepo{
		Name:        repoModel.Name,
		Description: repoModel.Description,
		Homepage:    repoModel.WebURL,
		License:     "",
		Stars:       repoModel.StarCount,
		Url:         repoModel.WebURL,
	}

	return awesomeRepo
}

package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/madflow/awesomo/config"
	"github.com/madflow/awesomo/model"
)

func GithubRepo(repoArg string, cfg config.Config) model.AwesomeRepo {
	repoUrl, err := url.Parse(repoArg)
	if err != nil {
		panic(err)
	}

	var githubApiBaseUrl string

	if cfg.Github.ApiUrl != "" {
		githubApiBaseUrl = cfg.Github.ApiUrl
	} else {
		githubApiBaseUrl = "https://api.github.com"
	}

	var githubApiRepos string = fmt.Sprintf("%s/repos", githubApiBaseUrl)

	githubApiUrl, err := url.Parse(githubApiRepos)
	if err != nil {
		panic(err)
	}

	repoApiUrl := url.URL{Scheme: githubApiUrl.Scheme, Host: githubApiUrl.Host, Path: githubApiUrl.Path + repoUrl.Path}

	client := &http.Client{}
	req, _ := http.NewRequest("GET", repoApiUrl.String(), http.NoBody)
	if cfg.Github.ApiKey != "" {
		req.Header.Set("Authorization", fmt.Sprintf("token %s", cfg.Github.ApiKey))
	}
	repoResp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer repoResp.Body.Close()
	body, err := io.ReadAll(repoResp.Body)
	if err != nil {
		panic(err)
	}

	var repoModel model.GithubRepo

	if err := json.Unmarshal(body, &repoModel); err != nil {
		panic(err)
	}

	awesomeRepo := model.AwesomeRepo{
		Name:        repoModel.Name,
		Description: repoModel.Description,
		Homepage:    repoModel.Homepage,
		License:     repoModel.License.Name,
		Stars:       repoModel.StargazersCount,
		Url:         repoModel.HTMLURL,
	}

	return awesomeRepo
}

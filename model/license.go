package model

type LicenseInfo struct {
	Year   string
	Holder string
}

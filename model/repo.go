package model

type AwesomeRepo struct {
	Name        string
	Description string
	Homepage    string
	License     string
	Stars       int
	Url         string
}

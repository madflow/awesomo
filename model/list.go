package model

type AwesomoList struct {
	Name         string       `json:"name"`
	Description  string       `json:"description"`
	Repositories []Repository `json:"repositories"`
	Settings     ListSettings `json:"settings"`
}

type ListSettings struct {
	GithubSettings *ListGithubSettings `json:"github"`
}

type ListGithubSettings struct {
	MinStars int `json:"min_stars"`
}

type Repository struct {
	Name    string `json:"name"`
	RepoUrl string `json:"repo_url"`
}

type ListItem struct {
	Name        string
	Description string
	Url         string
	Extra       string
}

type ListTemplate struct {
	Title       string
	Description string
	ListItems   []ListItem
}

type OutputFormat interface {
	String() string
	Set(string) error
	Type() string
}

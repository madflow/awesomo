FROM golang:1.23-alpine AS builder

WORKDIR /build

COPY . ./

RUN go generate ./...
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" . 

FROM golang:1.23-alpine

RUN apk add --no-cache bash \
  curl \
  git \
  tini

COPY --from=builder /build/awesomo /usr/local/bin/awesomo

COPY docker/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/sbin/tini", "--", "/entrypoint.sh"]
CMD [ "-h" ]


# Contribution Guidelines

## Pre-requisites

- Please only submit high quality resources.

## Making changes

- The best way to make changes to the project is to submit a Pull/Merge Request.
- Pull/Merge Request should be accompanied by a clear and concise description of the change.
- Only make changes in `list.json` file.
- Add/Remove/Change only one line in `list.json`.
- Do not edit any other file.
- Create a Pull/Merge Request and prefix the title with add:, remove:, change:.

package templates

import "embed"

//go:embed default/*
var DefaultTemplates embed.FS

//go:embed LICENSE.*.txt
var Licenses embed.FS

//go:embed *.md
var Readmes embed.FS

//go:embed *.yml
var Yamls embed.FS

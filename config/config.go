package config

type GithubConfig struct {
	ApiUrl string `mapstructure:"api_url"`
	ApiKey string `mapstructure:"api_key"`
}

type GitlabConfig struct {
	ApiUrl string `mapstructure:"api_url"`
	ApiKey string `mapstructure:"api_key"`
}

type Config struct {
	Github GithubConfig `mapstructure:"github"`
  Gitlab GitlabConfig `mapstructure:"gitlab"`
}

package cmd

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/go-rod/rod"
	"github.com/madflow/awesomo/handler"
	"github.com/madflow/awesomo/model"
	"github.com/spf13/cobra"
)

type ScrapeConfig struct {
	Url             string
	OutputDirectory string
}

var scrapeConfig ScrapeConfig

var scrapeCmd = &cobra.Command{
	Use:   "scrape",
	Short: "Scrape a website for supported links",
	Run: func(cmd *cobra.Command, args []string) {
		// check if putput directory exists
		_, err := os.Stat(scrapeConfig.OutputDirectory)
		if err != nil {
			log.Fatal("Output directory does not exist")
		}

		// validate url
		parsedUrl, error := url.ParseRequestURI(scrapeConfig.Url)
		if error != nil {
			fmt.Println("Error parsing url")
			return
		}

		var awesomeRepo model.AwesomeRepo
		var awesomeList model.AwesomoList = model.AwesomoList{}

		if parsedUrl.Hostname() == "github.com" {
			awesomeRepo = handler.GithubRepo(parsedUrl.String(), cfg)
			fmt.Println("Github repo:", awesomeRepo.Name)
		} else if parsedUrl.Hostname() == "gitlab.com" {
			awesomeRepo = handler.GitlabRepo(parsedUrl.String(), cfg)
		} else {
			log.Fatalf("There is no valid handler for hostname %s", parsedUrl.Hostname())
		}

		awesomeList.Name = awesomeRepo.Name
		awesomeList.Description = awesomeRepo.Description
		var reportUrls []model.Repository

		page := rod.New().MustConnect().MustPage(parsedUrl.String())
		if parsedUrl.Hostname() == "github.com" {
			page.MustElement("react-partial.loaded")
		} else {
			time.Sleep(5 * time.Second)
		}

		allLinks := page.MustElements("a")
		for _, link := range allLinks {
			href := link.MustAttribute("href")
			parsedHref, error := url.ParseRequestURI(*href)
			if error != nil {
				continue
			}
			if parsedHref.Hostname() == "github.com" {
				// remove forward slashes from beginning and end
				path := strings.Trim(parsedHref.Path, "/")
				pathsParts := strings.Split(path, "/")
				if len(pathsParts) > 1 {
					awesomeRepo = handler.GithubRepo(parsedHref.String(), cfg)
					if awesomeRepo.Url != "" {
						log.Printf("%s %s %s %s %s\n", awesomeRepo.Name, awesomeRepo.Url, awesomeRepo.Description, awesomeRepo.Homepage, awesomeRepo.License)
						reportUrls = append(reportUrls, model.Repository{RepoUrl: awesomeRepo.Url, Name: awesomeRepo.Name})
					}
				}
			}
		}
		awesomeList.Repositories = reportUrls

		awesomeListJson, _ := json.Marshal(awesomeList)
		// write to list.json to the output directory
		err = os.WriteFile(filepath.Join(scrapeConfig.OutputDirectory, "list.json"), awesomeListJson, 0o600)
		if err != nil {
			log.Fatal(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(scrapeCmd)
	// Url
	scrapeCmd.Flags().StringVar(&scrapeConfig.Url, "url", "", "the url to scrape")
	// The output directory shorcut -o
	scrapeCmd.Flags().StringVarP(&scrapeConfig.OutputDirectory, "output", "o", "", "the output directory")
	scrapeCmd.MarkFlagRequired("url")
	scrapeCmd.MarkFlagRequired("output")
}

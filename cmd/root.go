package cmd

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/madflow/awesomo/config"
	"github.com/madflow/awesomo/handler"
	"github.com/madflow/awesomo/model"
	"github.com/madflow/awesomo/templates"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type outputFormat string

const (
	outputFormatHtml     outputFormat = "html"
	outputFormatMarkdown outputFormat = "md"
)

// String is used both by fmt.Print and by Cobra in help text
func (e *outputFormat) String() string {
	return string(*e)
}

// Set must have pointer receiver so it doesn't change the value of a copy
func (e *outputFormat) Set(v string) error {
	switch v {
	case "html", "md":
		*e = outputFormat(v)
		return nil
	default:
		return errors.New(`must be one of "html", "md"`)
	}
}

// Type is only used in help text
func (e *outputFormat) Type() string {
	return "outputFormat"
}

var flagOutputFormat outputFormat = outputFormatMarkdown

var flagTemplate string

var cfgFile string

var cfg config.Config = config.Config{}

var rootCmd = &cobra.Command{
	Use:   "awesomo [flags] config",
	Short: "Awesomo awesome list generator",
	Long:  ``,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// The json file containing the list items
		configFileArg := args[0]

		configFile, err := os.Open(configFileArg)
		if err != nil {
			log.Fatal(err)
		}

		defer configFile.Close()

		configFileByteValue, _ := io.ReadAll(configFile)

		var awesomeList model.AwesomoList

		err = json.Unmarshal(configFileByteValue, &awesomeList)
		if err != nil {
			log.Fatal(err)
		}

		documentLines := []model.ListItem{}

		var repos []model.AwesomeRepo
		var awesomeRepo model.AwesomeRepo
		for i := 0; i < len(awesomeList.Repositories); i++ {
			var repoUrl string = awesomeList.Repositories[i].RepoUrl
			parsedRepoUrl, err := url.ParseRequestURI(repoUrl)
			if err != nil {
				log.Fatalf("Could not parse repo url %s", repoUrl)
			}

			if parsedRepoUrl.Hostname() == "github.com" {
				awesomeRepo = handler.GithubRepo(repoUrl, cfg)
			} else if parsedRepoUrl.Hostname() == "gitlab.com" {
				awesomeRepo = handler.GitlabRepo(repoUrl, cfg)
			} else {
				log.Fatalf("There is no valid handler for hostname %s", parsedRepoUrl.Hostname())
			}
			repos = append(repos, awesomeRepo)
		}

		sort.Slice(repos, func(i, j int) bool {
			return repos[i].Stars > repos[j].Stars
		})

		for i := 0; i < len(repos); i++ {
			repo := repos[i]
			if repo.Stars < awesomeList.Settings.GithubSettings.MinStars {
				continue
			}
			var extra strings.Builder
			if repo.Stars >= 0 {
				extra.WriteString(fmt.Sprintf("%d  ⭐ ", repo.Stars))
			}
			if repo.License != "" {
				extra.WriteString(repo.License + " ")
			}
			listItem := model.ListItem{Name: repo.Name, Description: repo.Description, Url: repo.Url, Extra: extra.String()}
			documentLines = append(documentLines, listItem)
		}

		var parsedTemplate *template.Template

		if flagTemplate != "" {

			var tpl string

			if flagOutputFormat == outputFormatHtml {
				tpl = filepath.Join(flagTemplate, "index.html")
			}

			if flagOutputFormat == outputFormatMarkdown {
				tpl = filepath.Join(flagTemplate, "README.md")
			}

			if _, err := os.Stat(tpl); errors.Is(err, os.ErrNotExist) {
				log.Fatalf("The template %s does not exist", tpl)
			}
			parsedTemplate, _ = template.ParseFiles(tpl)
		} else {
			if flagOutputFormat == outputFormatHtml {
				parsedTemplate, _ = template.ParseFS(templates.DefaultTemplates, "default/index.html")
			}
			if flagOutputFormat == outputFormatMarkdown {
				parsedTemplate, _ = template.ParseFS(templates.DefaultTemplates, "default/README.md")
			}
		}

		listTemplate := model.ListTemplate{Title: awesomeList.Name, Description: awesomeList.Description, ListItems: documentLines}
		err = parsedTemplate.Execute(os.Stdout, listTemplate)
		if err != nil {
			log.Fatal(err)
		}
	},
}

func PrettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.Flags().VarP(&flagOutputFormat, "output", "o", `output format allowed: "html", "md"`)
	rootCmd.Flags().StringVar(&flagTemplate, "template", "", "the template to be used")
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.awesomo.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Find current directory.
		current, _ := os.Getwd()
		// Search config in home directory with name ".awesomo" (without extension).
		viper.AddConfigPath(current)
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".awesomo")
	}

	err := viper.BindEnv("github.api_key", "GITHUB_API_KEY")
	if err != nil {
		log.Fatalf("Could not bind  github api key env variable")
	}

	err = viper.BindEnv("github.api_url", "GITHUB_API_URL")
	if err != nil {
		log.Fatalf("Could not bind github api url env variable")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}

	err = viper.Unmarshal(&cfg)
	if err != nil {
		log.Fatalf("Error unmarshalling config: %s", err)
	}
}

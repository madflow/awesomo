package cmd

import (
	"fmt"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check if everything is ok and ready to go",
	Run: func(cmd *cobra.Command, args []string) {
		client := &http.Client{}
		req, err := http.NewRequest("GET", cfg.Github.ApiUrl, http.NoBody)
		if err != nil {
			log.Fatalf("Could not create request: %v", err)
		}
		if cfg.Github.ApiKey != "" {
			req.Header.Set("Authorization", fmt.Sprintf("token %s", cfg.Github.ApiKey))
		}
		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		if resp.StatusCode == 401 {
			log.Fatal("The Github API is not properly authenticated")
		}

		fmt.Println("OK")
	},
}

func init() {
	rootCmd.AddCommand(checkCmd)
}

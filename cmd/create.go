package cmd

import (
	"fmt"
	"html/template"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/madflow/awesomo/model"
	"github.com/madflow/awesomo/templates"
	"github.com/spf13/cobra"
)

type CreateConfig struct {
	Directory string
}

var createConfig CreateConfig

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "create a new repository",
	Run: func(cmd *cobra.Command, args []string) {
		// check if directory exists
		_, err := os.Stat(createConfig.Directory)
		if err == nil {
			log.Fatal("Directory already exists")
		}
		err = os.Mkdir(createConfig.Directory, os.FileMode(0o755))
		if err != nil {
			log.Fatalf("Could not create directory: %s", err)
		}
		// create a README.md file in the directory

		readme, err := os.Create(filepath.Join(createConfig.Directory, "README.md"))
		if err != nil {
			log.Fatal(err)
		}
		defer readme.Close()

		// create a LICENSE file in the directory and add the MIT license
		// the template is in templates/LICENSE.MIT.txt

		licenceTpl, err := templates.Licenses.ReadFile("LICENSE.MIT.txt")
		if err != nil {
			log.Fatal(err)
		}
		parsedLicenseTpl, err := template.New("license").Parse(string(licenceTpl))
		if err != nil {
			log.Fatal(err)
		}
		licenseInfo := model.LicenseInfo{
			Year:   fmt.Sprintf("%d - %d", 2024, time.Now().Year()),
			Holder: "Madflow",
		}

		license, err := os.Create(filepath.Join(createConfig.Directory, "LICENSE"))
		if err != nil {
			log.Fatalf("Could not create LICENSE file: %s", err)
		}

		err = parsedLicenseTpl.Execute(license, licenseInfo)
		if err != nil {
			log.Fatalf("Could not execute template: %s", err)
		}
		defer license.Close()

		// copy CONTRIBUTING.md file from templates
		contributing, err := templates.Readmes.ReadFile("CONTRIBUTING.md")
		if err != nil {
			log.Fatalf("Could not read CONTRIBUTING.md file: %s", err)
		}

		err = os.WriteFile(filepath.Join(createConfig.Directory, "CONTRIBUTING.md"), contributing, os.FileMode(0o644))
		if err != nil {
			log.Fatalf("Could not write CONTRIBUTING.md file: %s", err)
		}

		// make sure there is a "public" directory
		err = os.Mkdir(filepath.Join(createConfig.Directory, "public"), os.FileMode(0o755))
		if err != nil {
			log.Fatalf("Could not create public directory: %s", err)
		}
		// put a .gitkeep in the public directory
		err = os.WriteFile(filepath.Join(createConfig.Directory, "public", ".gitkeep"), []byte{}, os.FileMode(0o644))
		if err != nil {
			log.Fatalf("Could not write .gitkeep file: %s", err)
		}

		// copy gitlab-ci-yml file from templates
		gitlabCiYml, err := templates.Yamls.ReadFile("gitlab-ci.yml")
		if err != nil {
			log.Fatalf("Could not read gitlab-ci.yml file: %s", err)
		}

		err = os.WriteFile(filepath.Join(createConfig.Directory, ".gitlab-ci.yml"), gitlabCiYml, os.FileMode(0o644))
		if err != nil {
			log.Fatalf("Could not write .gitlab-ci.yml file: %s", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(createCmd)

	createCmd.Flags().StringVarP(&createConfig.Directory, "directory", "d", "", "repository directory to create")
	createCmd.MarkFlagRequired("directory")
}
